#!/bin/sh
. "$(dirname "$0")/env.sh"

command -v mariadb >/dev/null || {
	echo "mariadb client not installed!"
	exit 1
}

printf 'Starting database...'
if podman run --rm -d \
	-p "$HOST:3306:3306" \
	-e MARIADB_DATABASE="$DATABASE" \
	-e MARIADB_USER="$USER" \
	-e MARIADB_PASSWORD="$PASSWORD" \
	-e MARIADB_ROOT_PASSWORD=rootpass \
	docker.io/mariadb:latest \
	>/dev/null
then
	printf ' OK!\nWaiting for database to start'
	while true; do
		mariadb \
			-u"$USER" \
			-p"$PASSWORD" \
			-h"$HOST" \
			--skip-ssl \
			"$DATABASE" \
			-e 'SHOW TABLES;' 2>/dev/null \
			&& break
		printf '.'
		sleep 1
	done
	printf ' Started!\nImporting data...'
	mariadb \
		-u"$USER" \
		-p"$PASSWORD" \
		-h"$HOST" \
		--skip-ssl \
		"$DATABASE" \
		< "$(dirname "$0")/DATABASE.sql" && printf ' OK\n'
else
	printf '\nFailed to start database container\n'
	exit 1
fi
