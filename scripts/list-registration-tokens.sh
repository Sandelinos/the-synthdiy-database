#!/bin/sh
. "$(dirname "$0")/env.sh"
mariadb \
	-h "$HOST" \
	-u "$USER" \
	-p"$PASSWORD" \
	--skip-ssl \
	"$DATABASE" \
	-e 'SELECT * FROM registration_tokens'
