#!/bin/sh
server="$1"
[ -z "$server" ] && {
	>&2 printf "Usage: %s <backend address>\n\nExample: %s 127.0.0.1:3000\n" \
		"$0" "$0"
	exit 1
}

set -e
insert_data() {
	if json=$(curl -s -X POST \
		-H 'Content-Type: application/json' \
		--cookie "sessionToken=$token" \
		"$server/$1" -d "$2")
	then
		echo "$json" | jq -r -e .id
	else
		>&2 echo "Failed to insert $1!"
		return 1
	fi
}

. "$(dirname "$0")/env.sh"
registration_token=$(base64 < /dev/urandom | head -c 16)
mariadb \
	-h "$HOST" \
	-u "$USER" \
	-p"$PASSWORD" \
	--skip-ssl \
	"$DATABASE" \
	-e 'INSERT INTO registration_tokens VALUES ('"'$registration_token'"')' \

# Register user
printf 'Registering user...'
register_data='{
	"user": "user",
	"password": "password",
	"registration_token": "'"$registration_token"'"
}'
response=$(curl -s -o /dev/null \
	-w '%{http_code}' \
	-H 'Content-Type: application/json' \
	-d "$register_data" \
	"$server/register"
)
if [ "$response" = '200' ]; then
	echo ' OK!'
elif [ "$response" = '400' ]; then
	echo ' User already exists.'
else
	echo ' Failed!'
	exit 1
fi

# Log in
printf 'Logging in...'
token=$(curl -s -o /dev/null \
	-w '%header{set-cookie}' \
	-H 'Content-Type: application/json' \
	-d '{"user": "user", "password": "password"}' \
	"$server/login" \
	| sed -n '/^sessionToken/s/^sessionToken=\([^;]\+\);\?.*/\1/p'
)
if [ -n "$token" ]; then
	echo ' OK!'
else
	echo ' Failed!'
	exit 1
fi

rene_creator_id=$(insert_data creator '{
	"name": "René Schmitz",
	"links": [{"type": "website", "url": "https://www.schmitzbits.de"}]
}')
kassu_creator_id=$(insert_data creator '{
	"brand": "Kassutronics",
	"links": [
		{"type": "website", "url": "https://kassu2000.blogspot.com"},
		{"type": "store", "url": "https://store.kassutronics.net/"},
		{"type": "git", "url": "https://github.com/kassu"}
	]
}')
sandelinos_creator_id=$(insert_data creator '{
	"name": "Sandelinos",
	"links": [{"type": "website", "url": "https://sandelinos.me"}]
}')
juanito_creator_id=$(insert_data creator '{
	"name": "Juanito Moore",
	"brand": "Modular for the Masses",
	"links": [
		{"type": "video", "url": "https://www.youtube.com/@ozerik"},
		{"type": "website", "url": "https://modularforthemasses.blogspot.com/"},
		{"type": "store", "url": "https://modularforthemasses.square.site/"},
		{"type": "git", "url": "https://github.com/ozerik/"}
	]
}')



rene_module_id=$(insert_data module '{
	"name": "Korg late MS20 filter",
	"creators": ["'"$rene_creator_id"'"],
	"url": "https://www.schmitzbits.de/ms20.html",
	"resources": [{"type": "schematic"}]
}')
kassu_module_id=$(insert_data module '{
	"name": "KS-20 Filter",
	"creators": ["'"$kassu_creator_id"'"],
	"parents": ["'"$rene_module_id"'"],
	"url": "https://kassu2000.blogspot.com/2019/07/ks-20-filter.html",
	"resources": [
		{
			"type": "schematic",
			"url": "https://github.com/kassu/kassutronics/tree/master/documentation/KS-20%20VCF"
		},
		{
			"type": "panel-template",
			"format": "eurorack",
			"url": "https://github.com/kassu/kassutronics/tree/master/documentation/KS-20%20VCF"
		}
	]
}')
sandelinos_module_id=$(insert_data module '{
	"name": "My stripboard KS-20 filter clone",
	"creators": ["'"$sandelinos_creator_id"'"],
	"parents": ["'"$kassu_module_id"'"],
	"url": "https://sandelinos.me/diy/ks-20-filter/",
	"resources": [
		{"type": "stripboard-layout"},
		{"type": "panel-template", "format": "eurorack"}
	]
}')
juanito_module_id=$(insert_data module '{
	"name": "MS-20 VCF",
	"creators": ["'"$juanito_creator_id"'"],
	"parents": ["'"$rene_module_id"'"],
	"url": "https://www.youtube.com/watch?v=uq3TvPBio_g",
	"resources": [
		{"type": "build-video"},
		{"type": "schematic", "url": "https://drive.google.com/open?id=1PnwspQndJK8oI6rkUyz9AkvxLnCkyd6k"}
	]
}')

echo "		Creator ID				Module ID
Rene Schmitz:	$rene_creator_id	$rene_module_id
Kassutronics:	$kassu_creator_id	$kassu_module_id
Sandelinos:	$sandelinos_creator_id	$sandelinos_module_id
Juanito:	$juanito_creator_id	$juanito_module_id"
