CREATE TABLE module (
	ID binary(16) NOT NULL,		# UUIDv7
	Name varchar(255) NOT NULL,	# name
	URL varchar(255) NOT NULL,	# URL
	PRIMARY KEY (ID)
);

CREATE TABLE module_creators (
	Module binary(16) NOT NULL,	# module UUIDv7
	Creator binary(16) NOT NULL	# creator UUIDv7
);

CREATE TABLE module_parents (
	Module binary(16) NOT NULL,	# module UUIDv7
	Parent binary(16) NOT NULL	# parent module UUIDv7
);

CREATE TABLE module_resources (
	Module binary(16) NOT NULL,	# module UUIDv7
	Type TINYINT UNSIGNED NOT NULL,	# type
	URL varchar(255),		# URL
	Format TINYINT UNSIGNED		# format
);

CREATE TABLE creator (
	ID binary(16) NOT NULL,		# UUIDv7
	Name varchar(255),		# name
	Brand varchar(255),		# brand
	PRIMARY KEY (ID)
);

CREATE TABLE creator_links (
	Creator binary(16) NOT NULL,	# creator UUIDv7
	Type TINYINT UNSIGNED NOT NULL,	# type
	URL varchar(255) NOT NULL	# URL
);

CREATE TABLE users (
	Username varchar(255) NOT NULL,
	Password varchar(255) NOT NULL,
	PRIMARY KEY (Username)
);

CREATE TABLE registration_tokens (
	Token varchar(255) NOT NULL,
	PRIMARY KEY (Token)
);
