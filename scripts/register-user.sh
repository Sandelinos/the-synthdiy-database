#!/bin/sh
server="$1"
username="$2"
token="$3"

[ "$#" -ne 3 ] && {
	>&2 printf "Usage: %s <backend address> <username> <token>\n\nExample: %s 127.0.0.1:3000 user secret_token\n" \
		"$0" "$0"
	exit 1
}

data='{
	"user": "'"$username"'",
	"password": "password",
	"registration_token": "'"$token"'"
}'

# Register user
printf 'Registering user...\n'
curl -s -v \
	-H 'Content-Type: application/json' \
	-d "$data" \
	"$server/register"
echo
