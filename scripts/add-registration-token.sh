#!/bin/sh
. "$(dirname "$0")/env.sh"
token=$(base64 < /dev/urandom | head -c 16)
mariadb \
	-h "$HOST" \
	-u "$USER" \
	-p"$PASSWORD" \
	--skip-ssl \
	"$DATABASE" \
	-e 'INSERT INTO registration_tokens VALUES ('"'$token'"')' \
	&& echo "$token"
