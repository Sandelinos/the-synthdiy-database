use serde::Deserialize;
use std::fs;

#[derive(Deserialize, Clone, Debug)]
pub struct Config {
	pub server: ConfigServer,
	pub database: ConfigDatabase,
	pub jwt: ConfigJwt,
	pub cors: ConfigCors,
}

#[derive(Deserialize, Clone, Debug)]
pub struct ConfigServer {
	pub listen_address: String,
}

#[derive(Deserialize, Clone, Debug)]
pub struct ConfigDatabase {
	pub user: String,
	pub password: String,
	pub host: String,
	pub database: String,
}

#[derive(Deserialize, Clone, Debug)]
pub struct ConfigJwt {
	pub secret: String,
}

#[derive(Deserialize, Clone, Debug)]
pub struct ConfigCors {
	pub frontend_address: String,
}

impl Config {
	pub fn new() -> Self {
		let config_path = std::env::args().nth(1).expect("No config file specified");
		let config_string = fs::read_to_string(config_path).expect("Failed to read configuration file");
		let config: Config = toml::from_str(&config_string).expect("Failed to parse configuration file");
		config
	}
}
