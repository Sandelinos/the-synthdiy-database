use crate::utils::jwt;
use crate::{APIError, AppState};
use argon2::{
	password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
	Argon2,
};
use axum::{
	extract::{self, State},
	http::StatusCode,
};
use serde::Deserialize;
use tracing::{debug, error, info};

#[derive(Deserialize, Debug)]
pub struct LoginRequest {
	user: String,
	password: String,
}

#[derive(Deserialize)]
pub struct RegistrationRequest {
	user: String,
	password: String,
	registration_token: String,
}

pub async fn login(
	state: State<AppState>,
	extract::Json(payload): extract::Json<LoginRequest>,
) -> Result<[(&'static str, String); 1], APIError> {
	// Get password hash from DB
	let password_hash: String =
		match sqlx::query_scalar("SELECT Password FROM users WHERE Username = ?")
			.bind(&payload.user)
			.fetch_optional(&state.pool)
			.await
		{
			Ok(Some(h)) => h,
			Ok(None) => {
				return Err(APIError {
					code: StatusCode::BAD_REQUEST,
					message: "User doesn't exist.".to_owned(),
				})
			}
			Err(e) => {
				debug!("Database error: {:?}", e);
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Database error.".to_owned(),
				});
			}
		};

	// Parse password hash
	let parsed_hash = match PasswordHash::new(&password_hash) {
		Ok(h) => h,
		Err(e) => {
			debug!("Argon2 error: {:?}", e);
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Failed to verify login.".to_owned(),
			});
		}
	};

	// Verify password hash
	if Argon2::default()
		.verify_password(payload.password.as_bytes(), &parsed_hash)
		.is_ok()
	{
		let token: String = match jwt::encode_token(&payload.user, &state.jwt_secret) {
			Ok(t) => t,
			Err(_) => {
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Failed to encode session token.".to_owned(),
				})
			}
		};
		info!("User {} logged in!", &payload.user);
		Ok([(
			"Set-Cookie",
			format!("sessionToken={token}; HttpOnly; Secure; SameSite=Strict"),
		)])
	} else {
		Err(APIError {
			code: StatusCode::UNAUTHORIZED,
			message: "Invalid credentials.".to_owned(),
		})
	}
}

pub async fn register(
	state: State<AppState>,
	extract::Json(payload): extract::Json<RegistrationRequest>,
) -> Result<(), APIError> {
	// Verify username length
	if payload.user.is_empty() {
		return Err(APIError {
			code: StatusCode::BAD_REQUEST,
			message: "Username must not be empty".to_owned(),
		});
	}

	// Verify password length
	if payload.password.len() < 8 {
		return Err(APIError {
			code: StatusCode::BAD_REQUEST,
			message: "Password must be at least 8 characters".to_owned(),
		});
	}

	// Verify registration token
	match sqlx::query("SELECT Token FROM registration_tokens WHERE Token = ?")
		.bind(&payload.registration_token)
		.fetch_optional(&state.pool)
		.await
	{
		Ok(Some(_)) => (),
		Ok(None) => {
			return Err(APIError {
				code: StatusCode::UNAUTHORIZED,
				message: "Invalid registration token.".to_owned(),
			})
		}
		Err(e) => {
			debug!("Database error: {:?}", e);
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Database error.".to_owned(),
			});
		}
	};

	// Hash password
	let salt = SaltString::generate(&mut OsRng);
	let argon2 = Argon2::default();
	let password_hash = match argon2.hash_password(payload.password.as_bytes(), &salt) {
		Ok(hash) => hash.to_string(),
		Err(_) => {
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Hashing password failed.".to_owned(),
			})
		}
	};

	// Insert user into database
	match sqlx::query("INSERT INTO users (Username, Password) VALUES (?, ?)")
		.bind(&payload.user)
		.bind(&password_hash)
		.execute(&state.pool)
		.await
	{
		Ok(_) => {
			info!("Registered user {}!", payload.user);

			// Delete registration token
			if sqlx::query("DELETE FROM registration_tokens WHERE Token = ?")
				.bind(&payload.registration_token)
				.execute(&state.pool)
				.await
				.is_err()
			{
				error!("Failed to delete used registration token.");
			};
			Ok(())
		}
		Err(sqlx::Error::Database(e)) if e.code().as_deref() == Some("23000") => {
			return Err(APIError {
				code: StatusCode::BAD_REQUEST,
				message: "User already exists.".to_owned(),
			})
		}
		Err(e) => {
			debug!("Database error: {:?}", e);
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Couldn't insert user into database.".to_owned(),
			});
		}
	}
}
