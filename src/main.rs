use axum::{
	http::{header::CONTENT_TYPE, HeaderValue},
	middleware,
	routing::{get, post},
	Router,
};
use sqlx::mysql::{MySqlConnectOptions, MySqlPool};
use tower_http::cors::CorsLayer;
use tracing::info;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
mod config;
use config::Config;
mod models;
mod utils;
use utils::api_error::APIError;
use utils::auth::auth;
mod login;
use login::{login, register};
mod routes;
use routes::{creator, module};

#[derive(Clone, Debug)]
pub struct AppState {
	pool: MySqlPool,
	jwt_secret: String,
}

#[tokio::main]
async fn main() {
	// setup logging
	tracing_subscriber::registry()
		.with(tracing_subscriber::EnvFilter::new(
			std::env::var("RUST_LOG").unwrap_or_else(|_| "the_synthdiy_database=debug".into()),
		))
		.with(tracing_subscriber::fmt::layer())
		.init();

	let config = Config::new();

	let pool_opts = MySqlConnectOptions::new()
		.host(&config.database.host)
		.username(&config.database.user)
		.password(&config.database.password)
		.database(&config.database.database);

	let pool = MySqlPool::connect_with(pool_opts)
		.await
		.expect("Unable to connect to database");
	info!(
		"Connected to database {}@{}/{}",
		config.database.user, config.database.host, config.database.database
	);

	let state = AppState {
		pool: pool,
		jwt_secret: config.jwt.secret,
	};

	let app = Router::new()
		.route("/module", post(module::add))
		.route("/creator", post(creator::add))
		.route_layer(middleware::from_fn_with_state(state.clone(), auth))
		.route("/module/:module_id", get(module::get))
		.route("/creator/:creator_id", get(creator::get))
		.route("/list_creators", get(creator::list))
		.route("/list_modules", get(module::list))
		.route("/login", post(login))
		.route("/register", post(register))
		.layer(CorsLayer::new()
			   .allow_origin(config.cors.frontend_address.parse::<HeaderValue>().unwrap())
			   .allow_headers([CONTENT_TYPE])
			   .allow_credentials(true)
		)
		.with_state(state);

	let listener = tokio::net::TcpListener::bind(config.server.listen_address)
		.await
		.expect("failed to start listener");
	info!(
		"Listening on address {}",
		listener
			.local_addr()
			.expect("Failed to get listener address.")
	);

	axum::serve(listener, app)
		.await
		.expect("failed to start server");
}
