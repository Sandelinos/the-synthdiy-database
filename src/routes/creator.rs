use crate::models::creator::{
	AddCreator, Creator, CreatorID, CreatorList, CreatorRow, Link, LinkRow,
};
use crate::{APIError, AppState};
use axum::{extract, extract::Path, extract::State, http::StatusCode, Extension, Json};
use tracing::{error, info};
use uuid::Uuid;

pub async fn add(
	state: State<AppState>,
	Extension(username): Extension<String>,
	extract::Json(payload): extract::Json<AddCreator>,
) -> Result<Json<CreatorID>, APIError> {
	let id = Uuid::now_v7();

	if payload.name == None && payload.brand == None {
		return Err(APIError {
			code: StatusCode::BAD_REQUEST,
			message: "a creator must have a name or a brand".to_owned(),
		});
	}

	// Start transaction
	let mut transaction = match state.pool.begin().await {
		Ok(t) => t,
		Err(_) => {
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Failed to start database transaction.".to_owned(),
			})
		}
	};

	// Insert creator
	match sqlx::query("INSERT INTO creator (ID, Name, Brand) VALUES (?, ?, ?)")
		.bind(&id.as_bytes()[..])
		.bind(&payload.name)
		.bind(&payload.brand)
		.execute(&mut *transaction)
		.await
	{
		Ok(_) => (),
		Err(_) => {
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Couldn't insert creator into database.".to_owned(),
			})
		}
	};

	// Insert creator links
	for link in &payload.links {
		match sqlx::query("INSERT INTO creator_links (Creator, Type, URL) VALUES (?, ?, ?)")
			.bind(&id.as_bytes()[..])
			.bind(u8::from(&link.link_type))
			.bind(&link.url)
			.execute(&mut *transaction)
			.await
		{
			Ok(_) => (),
			Err(_) => {
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Couldn't insert creator link(s) into database.".to_owned(),
				})
			}
		}
	}

	// Commit transaction
	match transaction.commit().await {
		Ok(_) => {
			info!("User {} added creator {}", username, id);
			Ok(Json(CreatorID { id: id }))
		}
		Err(_) => Err(APIError {
			code: StatusCode::INTERNAL_SERVER_ERROR,
			message: "Couldn't commit database transaction.".to_owned(),
		}),
	}
}

pub async fn get(
	state: State<AppState>,
	Path(creator_id): Path<Uuid>,
) -> Result<Json<Creator>, APIError> {
	let creator: CreatorRow = match sqlx::query_as("SELECT Name, Brand FROM creator WHERE ID = ?")
		.bind(&creator_id.as_bytes()[..])
		.fetch_optional(&state.pool)
		.await
	{
		Ok(Some(c)) => c,
		Ok(None) => {
			return Err(APIError {
				code: StatusCode::BAD_REQUEST,
				message: "Creator doesn't exist.".to_owned(),
			})
		}
		Err(_) => {
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Database error.".to_owned(),
			});
		}
	};

	let creator_link_rows: Vec<LinkRow> =
		match sqlx::query_as("SELECT Type, URL FROM creator_links WHERE Creator = ?")
			.bind(&creator_id.as_bytes()[..])
			.fetch_all(&state.pool)
			.await
		{
			Ok(l) => l,
			Err(_) => {
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Database error.".to_owned(),
				});
			}
		};

	let mut links = Vec::new();
	for link_row in creator_link_rows {
		match Link::try_from(&link_row) {
			Ok(link) => links.push(link),
			Err(_) => {
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Got invalid link type from database.".to_owned(),
				});
			}
		}
	}

	let result = Creator {
		id: creator_id,
		name: creator.name,
		brand: creator.brand,
		links: links,
	};
	Ok(Json(result))
}

pub async fn list(state: State<AppState>) -> Result<Json<Vec<CreatorList>>, APIError> {
	let creators: Vec<CreatorList> = match sqlx::query_as("SELECT ID, Name, Brand FROM creator")
		.fetch_all(&state.pool)
		.await
	{
		Ok(c) => c,
		Err(e) => {
			error!("Database error when listing creators: {}", e);
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Database error.".to_owned(),
			});
		}
	};
	Ok(Json(creators))
}
