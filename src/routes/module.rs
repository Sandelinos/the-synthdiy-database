use crate::models::creator::QueryCreator;
use crate::models::module::{AddModule, Module, ModuleID, ModuleList, ModuleRow, Resource};
use crate::{APIError, AppState};
use axum::{
	extract, extract::Path, extract::Query, extract::State, http::StatusCode, Extension, Json,
};
use tracing::{error, info};
use uuid::Uuid;

pub async fn add(
	state: State<AppState>,
	Extension(username): Extension<String>,
	extract::Json(payload): extract::Json<AddModule>,
) -> Result<Json<ModuleID>, APIError> {
	// Generate UUID for module
	let id = Uuid::now_v7();

	// Check if a creator is specified
	if payload.creators.len() < 1 {
		return Err(APIError {
			code: StatusCode::BAD_REQUEST,
			message: "No creator specified.".to_owned(),
		});
	};

	// Check if creator exists
	for creator in &payload.creators {
		match sqlx::query("SELECT ID FROM creator WHERE ID = ?")
			.bind(&creator.as_bytes()[..])
			.fetch_optional(&state.pool)
			.await
		{
			Ok(Some(_)) => {}
			Ok(None) => {
				return Err(APIError {
					code: StatusCode::BAD_REQUEST,
					message: "Creator does not exist.".to_owned(),
				})
			}
			Err(_) => {
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Something went wrong when checking creator.".to_owned(),
				})
			}
		};
	}

	// Start transaction
	let mut transaction = match state.pool.begin().await {
		Ok(t) => t,
		Err(_) => {
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Failed to start database transaction.".to_owned(),
			})
		}
	};

	// Insert module creators
	for creator in &payload.creators {
		match sqlx::query("INSERT INTO module_creators (Module, Creator) VALUES (?, ?)")
			.bind(&id.as_bytes()[..])
			.bind(&creator.as_bytes()[..])
			.execute(&mut *transaction)
			.await
		{
			Ok(_) => (),
			Err(_) => {
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Couldn't insert creator(s) into database.".to_owned(),
				})
			}
		};
	}

	// Insert module parents
	match &payload.parents {
		Some(parents) => {
			for parent in parents {
				match sqlx::query("INSERT INTO module_parents (Module, Parent) VALUES (?, ?)")
					.bind(&id.as_bytes()[..])
					.bind(&parent.as_bytes()[..])
					.execute(&mut *transaction)
					.await
				{
					Ok(_) => (),
					Err(_) => {
						return Err(APIError {
							code: StatusCode::INTERNAL_SERVER_ERROR,
							message: "Couldn't insert parent(s) into database.".to_owned(),
						})
					}
				};
			}
		}
		None => (),
	};

	// Insert module
	match sqlx::query("INSERT INTO module (ID, Name, URL) VALUES (?, ?, ?)")
		.bind(&id.as_bytes()[..])
		.bind(&payload.name)
		.bind(&payload.url)
		.execute(&mut *transaction)
		.await
	{
		Ok(_) => (),
		Err(_) => {
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Couldn't insert module into database.".to_owned(),
			})
		}
	};

	// Insert module resources
	for resource in &payload.resources {
		match sqlx::query(
			"INSERT INTO module_resources (Module, Type, URL, Format) VALUES (?, ?, ?, ?)",
		)
		.bind(&id.as_bytes()[..])
		.bind(u8::from(&resource.resource_type))
		.bind(&resource.url)
		.bind(&resource.format.as_ref().map(|f| u8::from(f)))
		.execute(&mut *transaction)
		.await
		{
			Ok(_) => (),
			Err(_) => {
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Couldn't insert resource(s) into database.".to_owned(),
				})
			}
		}
	}

	// Commit transaction
	match transaction.commit().await {
		Ok(_) => {
			info!("User {} added module {}", username, id);
			Ok(Json(ModuleID { id: id }))
		}
		Err(_) => Err(APIError {
			code: StatusCode::INTERNAL_SERVER_ERROR,
			message: "Couldn't commit database transaction.".to_owned(),
		}),
	}
}

pub async fn get(
	state: State<AppState>,
	Path(module_id): Path<Uuid>,
) -> Result<Json<Module>, APIError> {
	// Read module from DB
	let module: ModuleRow = match sqlx::query_as("SELECT Name, URL FROM module WHERE ID = ?")
		.bind(&module_id)
		.fetch_optional(&state.pool)
		.await
	{
		Ok(Some(m)) => m,
		Ok(None) => {
			return Err(APIError {
				code: StatusCode::BAD_REQUEST,
				message: "Module doesn't exist.".to_owned(),
			})
		}
		Err(e) => {
			tracing::error!("Database error: {}", e);
			return Err(APIError {
				code: StatusCode::INTERNAL_SERVER_ERROR,
				message: "Database error.".to_owned(),
			});
		}
	};

	// Read module creators from DB
	let module_creators: Vec<Uuid> =
		match sqlx::query_scalar("SELECT Creator FROM module_creators WHERE Module = ?")
			.bind(&module_id)
			.fetch_all(&state.pool)
			.await
		{
			Ok(c) => c,
			Err(e) => {
				tracing::error!("Database error: {}", e);
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Database error.".to_owned(),
				});
			}
		};

	// Read module parents from DB
	let module_parents: Vec<Uuid> =
		match sqlx::query_scalar("SELECT Parent FROM module_parents WHERE Module = ?")
			.bind(&module_id)
			.fetch_all(&state.pool)
			.await
		{
			Ok(p) => p,
			Err(e) => {
				tracing::error!("Database error: {}", e);
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Database error.".to_owned(),
				});
			}
		};

	// Read module resources from DB
	let module_resources: Vec<Resource> =
		match sqlx::query_as("SELECT Type, Format, URL FROM module_resources WHERE Module = ?")
			.bind(&module_id)
			.fetch_all(&state.pool)
			.await
		{
			Ok(r) => r,
			Err(e) => {
				tracing::error!("Database error: {}", e);
				return Err(APIError {
					code: StatusCode::INTERNAL_SERVER_ERROR,
					message: "Database error.".to_owned(),
				});
			}
		};

	let result = Module {
		id: module_id,
		name: module.name,
		creators: module_creators,
		parents: if module_parents.is_empty() { None } else { Some(module_parents) },
		url: module.url,
		resources: module_resources,
	};
	Ok(Json(result))
}

pub async fn list(
	state: State<AppState>,
	query: Query<QueryCreator>,
) -> Result<Json<Vec<ModuleList>>, APIError> {
	let modules: Vec<ModuleList> = match query.creator {
		Some(id) => {
			match sqlx::query_as(
				"SELECT ID, Name
				FROM module
				WHERE ID IN (
					SELECT module
					FROM module_creators
					WHERE Creator = ?
				);",
			)
			.bind(&id)
			.fetch_all(&state.pool)
			.await
			{
				Ok(m) => m,
				Err(e) => {
					error!("Database error when listing modules by creator: {}", e);
					return Err(APIError {
						code: StatusCode::INTERNAL_SERVER_ERROR,
						message: "Database error.".to_owned(),
					});
				}
			}
		}
		None => {
			match sqlx::query_as("SELECT ID, Name FROM module")
				.fetch_all(&state.pool)
				.await
			{
				Ok(m) => m,
				Err(e) => {
					error!("Database error when listing modules: {}", e);
					return Err(APIError {
						code: StatusCode::INTERNAL_SERVER_ERROR,
						message: "Database error.".to_owned(),
					});
				}
			}
		}
	};
	Ok(Json(modules))
}
