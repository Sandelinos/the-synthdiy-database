use serde::{Deserialize, Serialize};
use sqlx::{database::HasValueRef, Database, Decode, FromRow, MySql, Type};
use std::error::Error;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub enum ResourceType {
	Schematic,
	Gerber,
	PcbEtchingPrint,
	PanelTemplate,
	StripboardLayout,
	ProtoboardLayout,
	BuildVideo,
	Code,
}

impl From<&ResourceType> for u8 {
	fn from(resource_type: &ResourceType) -> Self {
		match resource_type {
			ResourceType::Schematic => 0,
			ResourceType::Gerber => 10,
			ResourceType::PcbEtchingPrint => 15,
			ResourceType::StripboardLayout => 20,
			ResourceType::ProtoboardLayout => 21,
			ResourceType::PanelTemplate => 40,
			ResourceType::BuildVideo => 60,
			ResourceType::Code => 70,
		}
	}
}

impl TryFrom<&u8> for ResourceType {
	type Error = &'static str;

	fn try_from(resource_type: &u8) -> Result<Self, Self::Error> {
		match resource_type {
			0 => Ok(ResourceType::Schematic),
			10 => Ok(ResourceType::Gerber),
			15 => Ok(ResourceType::PcbEtchingPrint),
			20 => Ok(ResourceType::StripboardLayout),
			21 => Ok(ResourceType::ProtoboardLayout),
			40 => Ok(ResourceType::PanelTemplate),
			60 => Ok(ResourceType::BuildVideo),
			70 => Ok(ResourceType::Code),
			_ => Err("Invalid resource type!"),
		}
	}
}

impl Type<MySql> for ResourceType {
	fn type_info() -> <MySql as Database>::TypeInfo {
		<u8 as Type<MySql>>::type_info()
	}
}

impl<'r> Decode<'r, MySql> for ResourceType {
	fn decode(
		value: <MySql as HasValueRef<'r>>::ValueRef,
	) -> Result<Self, Box<dyn Error + Send + Sync>> {
		let resource_type = <u8 as Decode<MySql>>::decode(value)?;
		ResourceType::try_from(&resource_type).map_err(Into::into)
	}
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub enum ResourceFormat {
	Eurorack,
	Kosmo,
}

impl From<&ResourceFormat> for u8 {
	fn from(format: &ResourceFormat) -> Self {
		match format {
			ResourceFormat::Eurorack => 0,
			ResourceFormat::Kosmo => 20,
		}
	}
}

impl TryFrom<&u8> for ResourceFormat {
	type Error = &'static str;

	fn try_from(resource_format: &u8) -> Result<Self, Self::Error> {
		match resource_format {
			0 => Ok(ResourceFormat::Eurorack),
			20 => Ok(ResourceFormat::Kosmo),
			_ => Err("Invalid resource format!"),
		}
	}
}

impl Type<MySql> for ResourceFormat {
	fn type_info() -> <MySql as Database>::TypeInfo {
		<u8 as Type<MySql>>::type_info()
	}
}

impl<'r> Decode<'r, MySql> for ResourceFormat {
	fn decode(
		value: <MySql as HasValueRef<'r>>::ValueRef,
	) -> Result<Self, Box<dyn Error + Send + Sync>> {
		let resource_format = <u8 as Decode<MySql>>::decode(value)?;
		ResourceFormat::try_from(&resource_format).map_err(Into::into)
	}
}

#[derive(Serialize, Deserialize, FromRow, Debug)]
pub struct Resource {
	#[serde(rename = "type")]
	#[sqlx(rename = "Type")]
	pub resource_type: ResourceType,
	#[serde(skip_serializing_if = "Option::is_none")]
	#[sqlx(rename = "Format")]
	pub format: Option<ResourceFormat>,
	#[serde(skip_serializing_if = "Option::is_none")]
	#[sqlx(rename = "URL")]
	pub url: Option<String>,
}

#[derive(Serialize)]
pub struct Module {
	pub id: Uuid,
	pub name: String,
	pub creators: Vec<Uuid>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub parents: Option<Vec<Uuid>>,
	pub url: String,
	pub resources: Vec<Resource>,
}

#[derive(Deserialize)]
pub struct AddModule {
	pub name: String,
	pub creators: Vec<Uuid>,
	pub parents: Option<Vec<Uuid>>,
	pub url: String,
	pub resources: Vec<Resource>,
}

#[derive(FromRow, Debug)]
pub struct ModuleRow {
	#[sqlx(rename = "Name")]
	pub name: String,
	#[sqlx(rename = "URL")]
	pub url: String,
}

#[derive(Serialize)]
pub struct ModuleID {
	pub id: Uuid,
}

#[derive(FromRow, Serialize)]
pub struct ModuleList {
	#[sqlx(rename = "ID")]
	pub id: Uuid,
	#[sqlx(rename = "Name")]
	pub name: String,
}
