use serde::{Deserialize, Serialize};
use sqlx::FromRow;
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum LinkType {
	Website,
	Git,
	Store,
	Video,
	Support,
}

impl From<&LinkType> for u8 {
	fn from(link_type: &LinkType) -> Self {
		match link_type {
			LinkType::Website => 0,
			LinkType::Git => 10,
			LinkType::Store => 20,
			LinkType::Video => 30,
			LinkType::Support => 40,
		}
	}
}

impl TryFrom<&u8> for LinkType {
	type Error = &'static str;

	fn try_from(link_type: &u8) -> Result<Self, Self::Error> {
		match link_type {
			0 => Ok(LinkType::Website),
			10 => Ok(LinkType::Git),
			20 => Ok(LinkType::Store),
			30 => Ok(LinkType::Video),
			40 => Ok(LinkType::Support),
			_ => Err("Invalid link type!"),
		}
	}
}

#[derive(FromRow)]
pub struct LinkRow {
	#[sqlx(rename = "Type")]
	pub type_int: u8,
	#[sqlx(rename = "URL")]
	pub url: String,
}

#[derive(Serialize, Deserialize)]
pub struct Link {
	#[serde(rename = "type")]
	pub link_type: LinkType,
	pub url: String,
}

impl TryFrom<&LinkRow> for Link {
	type Error = &'static str;

	fn try_from(link_row: &LinkRow) -> Result<Self, Self::Error> {
		let link_type: LinkType = match LinkType::try_from(&link_row.type_int) {
			Ok(l) => l,
			Err(_) => return Err("Row contains invalid link type!"),
		};
		Ok(Link {
			link_type: link_type,
			url: link_row.url.clone(),
		})
	}
}

#[derive(Serialize)]
pub struct Creator {
	pub id: Uuid,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub name: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub brand: Option<String>,
	pub links: Vec<Link>,
}

#[derive(Deserialize)]
pub struct AddCreator {
	pub name: Option<String>,
	pub brand: Option<String>,
	pub links: Vec<Link>,
}

#[derive(FromRow)]
#[sqlx(rename_all = "PascalCase")]
pub struct CreatorRow {
	pub name: Option<String>,
	pub brand: Option<String>,
}

#[derive(Serialize)]
pub struct CreatorID {
	pub id: Uuid,
}

#[derive(FromRow, Serialize, Debug)]
pub struct CreatorList {
	#[sqlx(rename = "ID")]
	pub id: Uuid,
	#[serde(skip_serializing_if = "Option::is_none")]
	#[sqlx(rename = "Name")]
	pub name: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	#[sqlx(rename = "Brand")]
	pub brand: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct QueryCreator {
	pub creator: Option<Uuid>,
}
