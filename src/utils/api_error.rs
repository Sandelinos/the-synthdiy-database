use axum::{
	http::StatusCode,
	response::{IntoResponse, Response},
	Json,
};
use serde_json::json;

pub struct APIError {
	pub message: String,
	pub code: StatusCode,
}

impl IntoResponse for APIError {
	fn into_response(self) -> Response {
		(self.code, Json(json!({ "errorMessage": self.message }))).into_response()
	}
}
