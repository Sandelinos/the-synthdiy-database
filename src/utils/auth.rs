use crate::utils::jwt;
use crate::{APIError, AppState};
use axum::{
	extract::{Request, State},
	http::StatusCode,
	middleware::Next,
	response::Response,
};

use axum_extra::extract::cookie::CookieJar;

pub async fn auth(
	state: State<AppState>,
	cookies: CookieJar,
	mut request: Request,
	next: Next,
) -> Result<Response, APIError> {
	match cookies.get("sessionToken") {
		Some(token) => {
			let username = jwt::verify_token(token.value(), &state.jwt_secret)?;
			request.extensions_mut().insert(username);
			let response = next.run(request).await;
			Ok(response)
		}
		None => Err(APIError {
			code: StatusCode::UNAUTHORIZED,
			message: "You are not logged in.".to_owned(),
		}),
	}
}
