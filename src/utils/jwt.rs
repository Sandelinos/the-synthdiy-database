use crate::APIError;
use axum::http::StatusCode;
use chrono::{TimeDelta, Utc};
use jsonwebtoken::{
	decode, encode, errors::ErrorKind, Algorithm, DecodingKey, EncodingKey, Header, Validation,
};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct Claims {
	exp: usize,
	sub: String,
}

pub fn encode_token(user: &str, secret: &str) -> Result<String, jsonwebtoken::errors::Error> {
	let claims = Claims {
		exp: (Utc::now() + TimeDelta::hours(2)).timestamp() as usize,
		sub: user.to_owned(),
	};

	encode(
		&Header::default(),
		&claims,
		&EncodingKey::from_secret(secret.as_bytes()),
	)
}

pub fn verify_token(token: &str, secret: &str) -> Result<String, APIError> {
	let decode_result = decode::<Claims>(
		token,
		&DecodingKey::from_secret(secret.as_bytes()),
		&Validation::new(Algorithm::HS256),
	);
	match decode_result {
		Ok(t) => Ok(t.claims.sub),
		Err(e) => match e.kind() {
			ErrorKind::ExpiredSignature => Err(APIError {
				code: StatusCode::UNAUTHORIZED,
				message: "Session expired. Log in again.".to_owned(),
			}),
			_ => Err(APIError {
				code: StatusCode::UNAUTHORIZED,
				message: "Invalid session token.".to_owned(),
			}),
		},
	}
}
