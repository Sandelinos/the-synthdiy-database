# The SynthDIY Database

There are tons of awesome DIY synthesizer module schematics, gerbers,
stripboard layouts and other resources out there, but they are scattered all
around the internet in people's blogs, videos, Git forges, forums etc.
The SynthDIY Database intends to index all of them into a simple
catalogue where you can search for links to modules based on what you're
looking for. For example, if your preferred method of building is on
stripboard, you could search for modules that have a stripboard layout available,
or if you're more into ordering PCBs and you're building a Kosmo modular, you
could search for modules that have a gerber available in the Kosmo format.

## Status

I have a basic draft of what information the service will contain and have
started implementing a backend based on it in Rust.

### Todo

- [ ] Specification
	- [ ] Add tags for modules. (like "sequencer", "VCF", "LFO" etc.)
- [x] Backend
	- [x] Authentication
	- [x] Adding modules & creators
	- [ ] Editing modules & creators
	- [x] Reading modules & creators
		- [x] Reading a single creator by ID
		- [x] Reading a single module by ID
	- [x] Listing modules & creators
		- [x] Listing modules by creator
## Frontend

I'm working on a simple html + js frontend
[here](https://codeberg.org/Sandelinos/The-SynthDIY-Database-js-frontend).

## Data

The service will contain information about creators and modules.

You can find some real-world examples in
[`example-data.json`](example-data.json). Below is some documentation about the
data structure.

### Creators

Here's an example JSON representation of a creator:

```
{
	"id": "018d2c52-0cea-7abe-8df4-da17d28b3cd3",
	"name": "John Smith",
	"brand": "Super Awesome Synthesizers",
	"links": [
		{ "type": "website", "url": "https://cool-synths.net" },
		{ "type": "git", "url": "https://git.sr.ht/~superawesomesynthesizers" }
	]
}
```

* `id`: UUIDv7 identifier of the creator. Modules use this to refer to their
  creator.
* `name`: (optional*) The creator's name.
* `brand`: (optional*) The creator's brand.
* `links`: (optional) List of the creator's links. (website, git profile etc.)
	* `type`: The link's type. Possible values:
		* `website`
		* `git`: The creator's user on a git forge.
		* `store`: A store where the creator sells things.
		* `video`: The creator's channel on a video site.
		* `support`: A page where you can support the creator, by donating
		  money or otherwise.
	* `url`: The URL.

\* A creator must have a name or a brand, or both.

### Modules

Here's an example JSON representation of a module:

```
{
	"id": "018d2c52-2328-76c6-8846-2aa7901359f9",
	"name": "A very cool module",
	"creators": ["018d2c52-0cea-7abe-8df4-da17d28b3cd3"],
	"parents": ["018d2c50-fe64-73ee-af4e-77af8c689ad8"],
	"url": "https://cool-synths.net/diy/a-very-cool-module",
	"resources": [
		{
			"type": "schematic"
		},
		{
			"type": "gerber",
			"format": "eurorack",
			"url": "https://somecloudservice.net/s/8ag97a9jj"
		}
	]
}
```

* `id`: UUIDv7 identifier of the module.
* `name`: Name of the module.
* `parents`: (optional) list of UUIDs of the modules that this one is based on.
* `creators`: List of UUIDs of the creators of the module. At least one creator
  must be specified.
* `url`: Link to the module's "main project page", which is the primary source
  of information about the module.
* `resources`: List of resources available for the module. At least one
  resource must be specified.
	* `type`: Type of the resource. Possible values:
		* `schematic`
		* `gerber`
		* `pcb-etching-print`
		* `panel-template`
		* `code`
		* `stripboard-layout`
		* `protoboard-layout`
		* `build-video`
	* `url`: (optional) Link to the resource. Only specify if the resource
	  is not found on the "main project page"
	* `format`: (optional) Module format. Only relevant for format-specific
	  stuff like gerbers and panel templates. Possible values:
		* `eurorack`
		* `kosmo`
